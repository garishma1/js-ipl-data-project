const csvToJSON = require('../utility');
const fs = require('fs');
function strikeRateOfABatsmanForEachSeason(){
    csvToJSON('../data/matches.csv').then((data) => {
        csvToJSON('../data/deliveries.csv').then((deliveriesData)=>{

            let batsmanData={}
            try{
                for(let matches of data){
                    for(let deliveries of deliveriesData){
                        if(matches.id === deliveries.match_id){
                           if(batsmanData[matches.season] === undefined){
                            batsmanData[matches.season]={}
                           }
                           if(deliveries.batsman in batsmanData[matches.season]){
                            batsmanData[matches.season][deliveries.batsman].runs += Number(deliveries.batsman_runs)
                            batsmanData[matches.season][deliveries.batsman].balls+=1
                           }else{
                            batsmanData[matches.season][deliveries.batsman]={ runs:Number(deliveries.batsman_runs), balls:1}
                           }
                        }
                    }
                }
            }catch(error){
                console.log(error)
            }
            let strikeRateOfbatsMan={}
            try{
                for(let season in batsmanData){
                    for(let batsman in batsmanData[season]){
                        if(strikeRateOfbatsMan[season]===undefined){
                            strikeRateOfbatsMan[season]={}
                        }
                        strikeRateOfbatsMan[season][batsman]=Number(((batsmanData[season][batsman].runs/batsmanData[season][batsman].balls)*100).toFixed(2))
                    }
                }
            }catch(error){
                console.log(error)
            }
            fs.writeFile('../public/output/strike_rate_of_a_batsman_for_each_season.json', JSON.stringify(strikeRateOfbatsMan) , (err) => {
                if(err){
                    console.log(err);
                }else{
                    console.log(' Problem completed and result has been stored');
                }
            })   
           
        })
    
    }) 
}       
strikeRateOfABatsmanForEachSeason()    