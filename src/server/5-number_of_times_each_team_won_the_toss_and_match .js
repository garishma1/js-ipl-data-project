const csvToJSON = require('../utility');
const fs = require('fs');

function numberOfTimesEachTeamWonTheTossAndmatch(){
    csvToJSON('../data/matches.csv').then((data) => {
        let numberOfTimesEachTeamWonTheTossAndmatch={}
        try{
            for(let element of data){
                if(element.toss_winner===element.winner){
                    if(element.winner in numberOfTimesEachTeamWonTheTossAndmatch){
                        numberOfTimesEachTeamWonTheTossAndmatch[element.winner]+=1
                    }else{
                        numberOfTimesEachTeamWonTheTossAndmatch[element.winner]=1
                    }
                }
            }
        }catch(error){
            console.log(error)
        }
        fs.writeFile('../public/output/number_of_times_each_team_won_the_toss_and_match.json', JSON.stringify(numberOfTimesEachTeamWonTheTossAndmatch) , (err) => {
            if(err){
                console.log(err);
            }else{
                console.log(' Problem completed and result has been stored');
            }
        })   
    })
}
numberOfTimesEachTeamWonTheTossAndmatch()

