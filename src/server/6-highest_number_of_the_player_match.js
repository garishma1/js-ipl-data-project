const csvToJSON = require('../utility');
const fs = require('fs');

function playerOfTheMatch(){
    csvToJSON('../data/matches.csv').then((data) => {
        let playerOfTheMatch={}
        try{
            for(let match of data){
                if(match.season in playerOfTheMatch){
                    if(match.player_of_match in playerOfTheMatch[match.season]){
                        playerOfTheMatch[match.season][match.player_of_match]+=1
                    }else{
                        playerOfTheMatch[match.season][match.player_of_match]=1
                    }
                }else{
                    playerOfTheMatch[match.season]={}
                    playerOfTheMatch[match.season][match.player_of_match]=1
                }
            }
        }catch(error){
            console.log(error)
        }
        let players = {}
        try{
            for(let match in playerOfTheMatch){
                let awards = 0
                for(let player in playerOfTheMatch[match]){
                    if(playerOfTheMatch[match][player]>awards){
                        players[match]=player
                        awards = playerOfTheMatch[match][player]
                    }
                }
            } 
        }catch(error){
            console.log(error)
        }
        fs.writeFile('../public/output/highest_number_of_the_player_match.json', JSON.stringify(players) , (err) => {
            if(err){
                console.log(err);
            }else{
                console.log(' Problem completed and result has been stored');
            }
        })   
        
    })
};
playerOfTheMatch()




