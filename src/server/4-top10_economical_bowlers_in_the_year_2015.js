const csvToJSON = require('../utility');
const fs = require('fs');

function top10EconomicalBowlers(){
   csvToJSON('../data/matches.csv').then((data)=>{
    let matches_id=[]
    try{
        for(let element of data){
            if(element.season === "2015"){
                matches_id.push(element.id)
            }
        }
    }catch(error){
        console.log(error)
    }
    csvToJSON('../data/deliveries.csv').then((deliveries)=>{
        let economicalBowlers={};
        try{
            for(let id of matches_id){
            for(let delivery of deliveries){
                if(id===delivery.match_id){
                    let bowler=delivery.bowler;
                    let totalRuns=Number(delivery.total_runs)
                    let byeRuns=Number(delivery.bye_runs)
                    let legbyeRuns=Number(delivery.legbye_runs)
                    let bowlerRuns=totalRuns-byeRuns-legbyeRuns

                    if(!economicalBowlers[bowler]){
                        economicalBowlers[bowler]={}
                        economicalBowlers[bowler]={runs:0,balls:0}
                    }

                    economicalBowlers[bowler].runs+=bowlerRuns
                    economicalBowlers[bowler].balls+=1
                }
            }
        
            }
        }catch(error){
        console.log(error)
        }
        let bowlersEconomy={}
        try{
            for(let bowler in economicalBowlers){
                let bowlerRuns=economicalBowlers[bowler].runs
                let bowlerBalls=economicalBowlers[bowler].balls
                let bowlerOvers =(bowlerBalls/6).toFixed(2)
                let economyOfBowler = (bowlerRuns/bowlerOvers).toFixed(2)
                bowlersEconomy[bowler]={economy:economyOfBowler}
    
            }
        }catch(error){
            console.log(error)
        }
        let arrayOfEconomicalBowlers=[]
        try{
            for(let bowler in bowlersEconomy){
                arrayOfEconomicalBowlers.push([bowler,bowlersEconomy[bowler]])
            } 
        }catch(error){
            console.log(error)
        }
        function bubbleSort(arr) {
            var len = arr.length;
            for (var i = 0; i < len - 1; i++) {
                for (var j = 0; j < len - 1 - i; j++) {
                    if (arr[j][1] > arr[j + 1]) {
                        var temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            return arr;
        }
        let bowlers = bubbleSort(arrayOfEconomicalBowlers)

        let tenEconomicalBowlers = []
        for(let index =0;index<10;index++){
            tenEconomicalBowlers.push(bowlers[index])
        }
        fs.writeFile('../public/output/top10_economical_bowlers_in_the_year_2015.json',JSON.stringify(tenEconomicalBowlers) , (err) => {
        if(err){
            console.log(err);
        }else{
            console.log(' Problem completed and result has been stored');
        }
    })   
        
    })   
 })
}
top10EconomicalBowlers()