const csvToJSON = require('../utility');
const fs = require('fs');

function extraRunsPerTeamInTheYear2016(){
    csvToJSON('../data/matches.csv').then((data) => {
        let matches_id=[]
        try{
            for(let element of data){
                if(element.season === "2016"){
                    matches_id.push(element.id)
                }
            }
        }catch(error){
            console.log(error)
        }
        csvToJSON('../data/deliveries.csv').then((deliveries)=>{
            let extraRuns = {}
            try{
                for(let match_id of matches_id ){
                    for(let element of deliveries){
                        if(match_id === element.match_id){
                            if(element.bowling_team in extraRuns){
                                extraRuns[element.bowling_team]+=+(element.extra_runs)
                            }else{
                                extraRuns[element.bowling_team]=+(element.extra_runs)
                            }
                        }
                    }
                }
            }catch(error){
                console.log(error)
            }
            fs.writeFile('../public/output/extra_runs_per_team_in_the_year_2016.json', JSON.stringify(extraRuns),(err) => {
                if(err){
                    console.log(err);
                }else{
                    console.log(' Problem completed and result has been stored');
                }
            })   
        })
    })
}
extraRunsPerTeamInTheYear2016()