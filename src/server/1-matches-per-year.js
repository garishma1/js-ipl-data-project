const csvToJSON = require('../utility');
const fs = require('fs');

function matchesPerYear(){
    csvToJSON('../data/matches.csv').then((data) => {
        
        let matchesPerYear ={}
        try{
            for(let element of data){
                if(element.season in matchesPerYear){
                    matchesPerYear[element.season]=matchesPerYear[element.season]+1
                }else{
                    matchesPerYear[element.season]=1
                }
            }
        }catch(error){
            console.log(error)
        }
        fs.writeFile('../public/output/matches_per_year.json', JSON.stringify(matchesPerYear) , (err) => {
            if(err){
                console.log(err);
            }else{
                console.log(' Problem completed and result has been stored');
            }
        })   
        
    })
};

matchesPerYear();