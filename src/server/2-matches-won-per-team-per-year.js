const csvToJSON = require('../utility');
const fs = require('fs');

function matchesWonPerTeamPerYear(){
    csvToJSON('../data/matches.csv').then((data) => {
        
        let matchesWonPerTeamPerYear ={}
        try{
            for(let element of data){
                if(element.season in matchesWonPerTeamPerYear){
                 if(element.winner in matchesWonPerTeamPerYear[element.season]){
                     matchesWonPerTeamPerYear[element.season][element.winner]=matchesWonPerTeamPerYear[element.season][element.winner]+1
                 }else{
                     matchesWonPerTeamPerYear[element.season][element.winner]=1
                 }
                }else{
                 matchesWonPerTeamPerYear[element.season]={}
                 matchesWonPerTeamPerYear[element.season][element.winner]=1
                }
            }
        }catch(error){
            console.log(error)
        }
        fs.writeFile('../public/output/matches_won_per_team_per_year.json', JSON.stringify(matchesWonPerTeamPerYear) , (err) => {
            if(err){
                console.log(err);
            }else{
                console.log(' Problem completed and result has been stored');
            }
        })   
        
    })
};
matchesWonPerTeamPerYear();