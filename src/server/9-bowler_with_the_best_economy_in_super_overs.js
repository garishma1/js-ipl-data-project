const csvToJSON = require('../utility');
const fs = require('fs');

function bowlerWithTheBestEconomyInSuperOvers(){
    csvToJSON('../data/deliveries.csv').then((deliveriesData)=>{
        let bowlerWithEconomy = {}
        try{
            for(let delivery of deliveriesData){
                if(delivery.is_super_over==="1"){
                    let bowler = delivery.bowler
                    let totalRuns=Number(delivery.total_runs)
                    let byeRuns=Number(delivery.bye_runs)
                    let legbyeRuns=Number(delivery.legbye_runs)
                    let bowlerRuns=totalRuns-byeRuns-legbyeRuns

                    if(!bowlerWithEconomy[bowler]){
                       bowlerWithEconomy[bowler]={}
                       bowlerWithEconomy[bowler]={runs:0,balls:0}
                    }

                    bowlerWithEconomy[bowler].runs+=bowlerRuns
                    bowlerWithEconomy[bowler].balls+= 1
                }
            }
        }catch(error){
            console.log(error)
        }
        let bowlersEconomy={}
        try{
            for(let bowler in bowlerWithEconomy){
                let bowlerRuns=bowlerWithEconomy[bowler].runs
                let bowlerBalls=bowlerWithEconomy[bowler].balls
                let bowlerOvers =(bowlerBalls/6).toFixed(2)
                let economyOfBowler = (bowlerRuns/bowlerOvers).toFixed(2)
                bowlersEconomy[bowler]={economy:economyOfBowler}
    
            }
        }catch(error){
            console.log(error)
        }
        let arrayOfBestEconomyInSuperOvers=[]
        try{
            for(let bowler in bowlersEconomy){
                arrayOfBestEconomyInSuperOvers.push([bowler,bowlersEconomy[bowler]])
            }
        }catch(error){
            console.log(error)
        }
        let bowlers = arrayOfBestEconomyInSuperOvers.sort((a,b)=>{
            return a[1].economy-b[1].economy
        })

        let bowlerWithBestEconomy = bowlers.slice(0,1)
        fs.writeFile('../public/output/bowler_with_the_nest_economy_in_super_overs.json',JSON.stringify(bowlerWithBestEconomy),(err)=>{
        if(err){
            console.log(err);
        }else{
            console.log(' Problem completed and result has been stored');
        }
        })   
        
    })   
 
}

bowlerWithTheBestEconomyInSuperOvers()