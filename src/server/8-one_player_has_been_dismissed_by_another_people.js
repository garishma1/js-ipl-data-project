const csvToJSON = require('../utility');
const fs = require('fs');

function onePlayerHasBeenDismissedByAnotherPeople(){
    csvToJSON('../data/deliveries.csv').then((deliveriesData)=>{
        let players={}
        try{
            for(let deliveries of deliveriesData){
                let dismissed=deliveries.player_dismissed
                 let bowler = deliveries.bowler
                 if(dismissed!==""){
                    if(dismissed in players){
                        if(bowler in players[dismissed]){
                            players[dismissed][bowler]+=1
                        }else{
                            players[dismissed][bowler]=1
                        }
                    }else{
                        players[dismissed]={}
                        players[dismissed][bowler]=1
                        
                        
                    }
                 }
            }
            
        }catch(error){
            console.log(error)
        }
        let highestnumber={}
        try{
            for(let batsman in players){
                let value=0
                for(let bowler in players[batsman]){
                    if(players[batsman][bowler]>value){
                        value=players[batsman][bowler]
                        highestnumber[batsman]={}
                        highestnumber[batsman][bowler]=players[batsman][bowler]
                    }
                }
            }
        }catch(error){
            console.log(error)

        }
        let dismissals={batsman:undefined,bowler:undefined,count:undefined}
        let number = 0
        try{
            for(let batsman in highestnumber){
                for(let bowler in highestnumber[batsman]){
                    if(highestnumber[batsman][bowler]>number){
                        number = highestnumber[batsman][bowler]
                        dismissals.batsman=batsman
                        dismissals.bowler=bowler
                        dismissals.count=highestnumber[batsman][bowler]
    
                    }
                }
    
            } 
        }catch(error){
            console.log(error)
        }
        
        console.log(dismissals)
        fs.writeFile('../public/output/one_player_has_been_dismissed_by_another_people.json',JSON.stringify(dismissals),(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log(' Problem completed and result has been stored');
            }
        })   
    })
}
onePlayerHasBeenDismissedByAnotherPeople()